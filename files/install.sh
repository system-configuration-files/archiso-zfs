#!/bin/bash
mount -o remount,size=5G /run/archiso/cowspace
mkdir installer
echo -e "root\nroot" | passwd 
systemctl start sshd
cd /root/installer && git clone https://gitlab.com/system-configuration-files/arch-zfs-ansible-install.git
cd /root/installer/arch-zfs-ansible-install && ansible-playbook --connection=local --inventory 127.0.0.1, base.yml
