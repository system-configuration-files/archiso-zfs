#!/bin/sh

# Copy basline files
cp -r /usr/share/archiso/configs/releng/ ~/archlive
cp /root/install.sh ~/archlive/airootfs/root/install.sh

# Add ZFS Repo
echo -e "\n[archzfs]\nServer = http://archzfs.com/\$repo/x86_64\nServer = http://mirror.sum7.eu/archlinux/archzfs/\$repo/x86_64\nSigLevel = Optional Never" >> ~/archlive/pacman.conf

# Add packages
echo "archzfs-dkms" >> ~/archlive/packages.x86_64
echo "linux-headers" >> ~/archlive/packages.x86_64
echo "ansible" >> ~/archlive/packages.x86_64 
echo "git" >> ~/archlive/packages.x86_64
echo "python-jmespath" >> ~/archlive/packages.x86_64

# Install on boot
sed -i '$ s/$/ script\=\/root\/install.sh/' ~/archlive/efiboot/loader/entries/archiso-x86_64-cd.conf
sed -i '/APPEND/ s/$/ script\=\/root\/install.sh/' ~/archlive/syslinux/archiso_sys.cfg

# Build ISO
mkdir ~/archlive/out
mkdir ~/archlive/wkdir
yes Y | mkarchiso -v -w ~/archlive/wkdir -o ~/archlive/out ~/archlive

# Move the final ISO to /tmp to be accessible from the host
mv ~/archlive/out/* /tmp
