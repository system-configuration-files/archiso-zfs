##
# NAME             : archiso-builder
# TO_BUILD         : docker build --rm -t archiso-builder:latest .
# TO_RUN           : docker run --rm -v /tmp:/tmp -t -i --privileged nlhomme/archiso-builder:latest 
##

FROM archlinux

#Install git and archiso
RUN pacman -Syyu --noconfirm
RUN pacman -S git archiso --noconfirm

#Copy the build script and allow him to be executed
COPY files/buildscript.sh ~/
COPY files/install.sh /root/

#Place the terminal in the home folder
RUN ["chmod", "+x", "~/buildscript.sh"]
RUN ["chmod", "+x", "/root/install.sh"]

ENTRYPOINT ["~/buildscript.sh"]

